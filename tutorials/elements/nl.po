# Dutch translations for Inkscape.
# This file is distributed under the same license as the Inkscape package.
#
# Kris De Gussem <kris.DeGussem@gmail.com>, 2010.
#
# *** Stuur een mailtje
# *** voordat je met dit bestand aan de slag gaat,
# *** om dubbel werk en stijlbreuken te voorkomen.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2011-07-16 12:50+0100\n"
"Last-Translator: Kris De Gussem <Kris.DeGussem@gmail.com>\n"
"Language-Team: Dutch\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Dutch\n"
"X-Poedit-Country: NETHERLANDS\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Kris De Gussem <kris.DeGussem@gmail.com>, 2010"

#. (itstool) path: articleinfo/title
#: tutorial-elements.xml:6
#, fuzzy
msgid "Elements of design"
msgstr "Ontwerpbeginselen"

#. (itstool) path: articleinfo/subtitle
#: tutorial-elements.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-elements.xml:11
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"Deze handleiding demonstreert de elementen en ontwerpbeginselen die normaal "
"aan kunststudenten aangeleerd worden om de verschillende aspecten in kunst "
"te begrijpen. Dit is geen exclusieve lijst, dus voeg toe, haal uit en "
"combineer om deze handleiding meer omvattend te maken."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:25
msgid "Elements of Design"
msgstr "Ontwerpbeginselen"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:26
msgid "The following elements are the building blocks of design."
msgstr "De volgende elementen zijn de bauwstenen van een ontwerp."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:30 elements-f01.svg:127
#, no-wrap
msgid "Line"
msgstr "Lijn"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:31
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"Een lijn is gedefinieerd als een merkteken met lengte en richting, gemaakt "
"door een punt dat langs een oppervlak beweegt. Een lijn kan variëren in "
"lengte, breedte, richting, kromming en kleur. Een lijn kan tweedimensionaal "
"(een potloodlijn op papier) of impliciet 3D zijn."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:45 elements-f01.svg:140
#, no-wrap
msgid "Shape"
msgstr "Vorm"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:46
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""
"Een vorm wordt gemaakt wanneer lijnen elkaar snijden en een ruimte omgeven. "
"Een verandering in kleur of schaduw kan een vorm definiëren. Vormen kunnen "
"verdeeld worden in verschillende types: geometrisch (vierkant, driehoek, "
"cirkel) of organisch (onregelmatig)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:60 elements-f01.svg:192
#, no-wrap
msgid "Size"
msgstr "Grootte"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:61
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"Grootte refereert naar variaties in de proporties van objecten, lijnen of "
"vormen. Er is zowel werkelijke als veronderstelde variatie in objectgrootte."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:74 elements-f01.svg:153
#, no-wrap
msgid "Space"
msgstr "Ruimte"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:75
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"Ruimte zijn de lege of open gebieden tussen, rond, boven, onder of in "
"objecten. Vormen worden bepaald door de ruimte rondom en binnenin ze. Ruimte "
"wordt vaak drie- of tweedimensionaal genoemd. Positieve ruimte wordt gevuld "
"door een vorm. Negatieve ruimte omgeeft een vorm."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:89 elements-f01.svg:114
#, no-wrap
msgid "Color"
msgstr "Kleur"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:90
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"Kleur is het gepercipieerde karakter van een oppervlak overeenkomstig de "
"golflente van het reflecterende licht. Kleur heeft drie dimensies: tint "
"(ander woord voor kleur, aangeduid door zijn naam zoals rood of geel), "
"verzadiging (hoeveelheid kleur) en intensiteit (helder of mat)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:104 elements-f01.svg:166
#, no-wrap
msgid "Texture"
msgstr "Textuur"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:105
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"Textuur is hoe een oppervlak aanvoelt (actuele textuur) of eruit ziet "
"(veronderstelde textuur). Texturen worden beschreven door woorden als ruw, "
"zijde- of kiezelachtig."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:118 elements-f01.svg:179
#, no-wrap
msgid "Value"
msgstr "Helderheid"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:119
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"Helderheid is hoe donker of licht iets eruit ziet. Veranderingen in "
"helderheid worden bereikt door zwart of wit aan de kleur toe te voegen. "
"Clair-obscur gebruikt helderheid in afbeeldingen voor het dramatisch "
"verhogen van contrasten in een compositie."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:133
msgid "Principles of Design"
msgstr "Ontwerpprincipes"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:134
msgid "The principles use the elements of design to create a composition."
msgstr "De principes gebruiken de ontwerpelementen om een compositie te maken."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:138 elements-f01.svg:205
#, no-wrap
msgid "Balance"
msgstr "Balans"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:139
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""
"Balans is het gevoel van visuele gelijkheid in vorm, helderheid, kleur, etc. "
"Balans kan symmetrisch of asymmetrisch zijn. Objecten, helderheid, kleuren, "
"texturen, vormen, etc. kunnen gebruikt worden voor de balans in een "
"compositie."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:153 elements-f01.svg:218
#, no-wrap
msgid "Contrast"
msgstr "Contrast"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:154
msgid "Contrast is the juxtaposition of opposing elements"
msgstr "Contrast is de nevenschikking van tegengestelde elementen."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:166 elements-f01.svg:231
#, no-wrap
msgid "Emphasis"
msgstr "Klemtoon"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:167
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"Klemtoon wordt gebruikt om bepaalde delen van het kunstwerk te accentueren "
"en je aandacht te trekken. Het middelpunt van de aandacht of focaal punt is "
"de plaats in een werk waar je oog het eerst naar kijkt."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:180 elements-f01.svg:244
#, no-wrap
msgid "Proportion"
msgstr "Proportie"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:181
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""
"Proportie beschrijft de grootte, plaats of hoeveelheid van een ding in "
"vergelijking met een ander."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:193 elements-f01.svg:257
#, no-wrap
msgid "Pattern"
msgstr "Patroon"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:194
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""
"Een patroon wordt gemaakt door het steeds herhalen van een element (lijn, "
"vorm of kleur)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:206 elements-f01.svg:271
#, no-wrap
msgid "Gradation"
msgstr "Gradatie"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:207
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"Gradatie in grootte en richting zorgt voor een lineair perspectief. Gradatie "
"in kleur van warm naar koud en toon van donker naar licht produceert een "
"atmosferisch perspectief. Gradatie kan voor interesse in en beweging van een "
"vorm zorgen. Een gradatie van donker naar licht laat het oog bewegen langs "
"de vorm."

#. (itstool) path: sect1/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:222 elements-f01.svg:284
#, no-wrap
msgid "Composition"
msgstr "Compositie"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:223
msgid "The combining of distinct elements to form a whole."
msgstr "Compositie is de combinatie van verschillende elementen in een geheel."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:235
msgid "Bibliography"
msgstr "Bibliografie"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:236
msgid "This is a partial bibliography used to build this document."
msgstr ""
"Dit is een gedeeltelijke bibliografie die gebruikt is voor het maken van dit "
"document."

#. (itstool) path: listitem/para
#: tutorial-elements.xml:241
msgid ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:246
msgid ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2.htm"
"\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:251
msgid ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:256
msgid ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:261
msgid ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-elements.xml:266
#, fuzzy
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink url="
"\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this tutorial. "
"Also, thanks to the Open Clip Art Library (<ulink url=\"http://www."
"openclipart.org/\">http://www.openclipart.org/</ulink>) and the graphics "
"people have submitted to that project."
msgstr ""
"Speciale dank gaat naar Linda Kim (<ulink url=\"http://www.redlucite.org"
"\">http://www.redlucite.org</ulink>) voor de hulp (<ulink url=\"http://www."
"rejon.org/\">http://www.rejon.org/</ulink>) bij deze handleiding. Verder ook "
"dank aan de Open Clip Art Library (<ulink url=\"http://www.openclipart.org/"
"\">http://www.openclipart.org/</ulink>) en de mensen die hun afbeeldingen "
"opgeladen hebben op dat project."

#. (itstool) path: Work/format
#: elements-f01.svg:48 elements-f02.svg:48 elements-f03.svg:48
#: elements-f04.svg:48 elements-f05.svg:48 elements-f06.svg:91
#: elements-f07.svg:91 elements-f08.svg:206 elements-f09.svg:48
#: elements-f10.svg:48 elements-f11.svg:48 elements-f12.svg:445
#: elements-f13.svg:102 elements-f14.svg:173 elements-f15.svg:452
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: elements-f01.svg:88
#, no-wrap
msgid "Elements"
msgstr "Elementen"

#. (itstool) path: text/tspan
#: elements-f01.svg:101
#, no-wrap
msgid "Principles"
msgstr "Principes"

#. (itstool) path: text/tspan
#: elements-f01.svg:297
#, no-wrap
msgid "Overview"
msgstr "Overzicht"

#. (itstool) path: text/tspan
#: elements-f04.svg:79
#, no-wrap
msgid "BIG"
msgstr "GR"

#. (itstool) path: text/tspan
#: elements-f04.svg:93
#, no-wrap
msgid "small"
msgstr "klein"

#. (itstool) path: text/tspan
#: elements-f12.svg:710
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Random mier &amp; 4x4"

#. (itstool) path: text/tspan
#: elements-f12.svg:721
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "SVG-afbeelding gemaakt door Andrew Fitzsimon"

#. (itstool) path: text/tspan
#: elements-f12.svg:726
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "Met dank aan de Open Clip Art Library"

#. (itstool) path: text/tspan
#: elements-f12.svg:731
#, no-wrap
msgid "http://www.openclipart.org/"
msgstr "http://www.openclipart.org/"

#~ msgid "http://www.makart.com/resources/artclass/EPlist.html"
#~ msgstr "http://www.makart.com/resources/artclass/EPlist.html"

#~ msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
#~ msgstr "http://www.princetonol.com/groups/iad/Files/elements2.htm"

#~ msgid "http://www.johnlovett.com/test.htm"
#~ msgstr "http://www.johnlovett.com/test.htm"

#~ msgid "http://digital-web.com/articles/elements_of_design/"
#~ msgstr "http://digital-web.com/articles/elements_of_design/"

#~ msgid "http://digital-web.com/articles/principles_of_design/"
#~ msgstr "http://digital-web.com/articles/principles_of_design/"

#~ msgid "http://sanford-artedventures.com/study/study.html"
#~ msgstr "http://sanford-artedventures.com/study/study.html"

#~ msgid "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"
#~ msgstr "http://oswego.org/staff/bpeterso/web/elements_and_principles.htm"

# Traditional Chinese Messages for inkscape.
# Copyright (C) 2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the inkscape package.
#
# Dong-Jun Wu <ziyawu@gmail.com>, 2011, 2012, 2016.
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Interpolate\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2021-05-11 17:05+0800\n"
"Last-Translator: Dongjun Wu <ziyawu@gmail.com>\n"
"Language-Team: Chinese <zh-l10n@linux.org.tw>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Poedit 2.4.1\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "dongjun wu <ziyawu@gmail.com>, 2009, 2014,2021"

#. (itstool) path: articleinfo/title
#: tutorial-interpolate.xml:6
msgid "Interpolate"
msgstr "內插"

#. (itstool) path: articleinfo/subtitle
#: tutorial-interpolate.xml:7
msgid "Tutorial"
msgstr "指導手冊"

#. (itstool) path: abstract/para
#: tutorial-interpolate.xml:11
msgid "This document explains how to use Inkscape's Interpolate extension"
msgstr "這篇文章說明如何使用 Inkscape 的內插擴充功能"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:16
msgid "Introduction"
msgstr "介紹"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:17
msgid ""
"Interpolate does a <firstterm>linear interpolation</firstterm> between two "
"or more selected paths. It basically means that it “fills in the gaps” "
"between the paths and transforms them according to the number of steps given."
msgstr ""
"內插是在兩個或多個選取路徑之間作<firstterm>線性內插</firstterm>。大致上來說就"
"是在路徑的間隔填入東西並且根據給定的階層數轉變它們。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:22
msgid ""
"To use the Interpolate extension, select the paths that you wish to "
"transform, and choose <menuchoice><guimenu>Extensions</"
"guimenu><guisubmenu>Generate From Path</guisubmenu><guimenuitem>Interpolate</"
"guimenuitem></menuchoice> from the menu."
msgstr ""
"開始使用內插擴充功能，選取你想要轉變的路徑，並從選單中選擇"
"<menuchoice><guimenu>擴充功能</guimenu><guisubmenu>從路徑產生</"
"guisubmenu><guimenuitem>內插</guimenuitem></menuchoice>。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:27
msgid ""
"Before invoking the extension, the objects that you are going to transform "
"need to be <emphasis>paths</emphasis>. This is done by selecting the object "
"and using <menuchoice><guimenu>Path</guimenu><guimenuitem>Object to Path</"
"guimenuitem></menuchoice> or <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>C</keycap></"
"keycombo>. If your objects are not paths, the extension will do nothing."
msgstr ""
"調用此擴充功能前，你必須先將物件轉換成<emphasis>路徑</emphasis>。選取物件然後"
"使用 <menuchoice><guimenu>路徑</guimenu><guimenuitem>物件轉成路徑</"
"guimenuitem></menuchoice> 或 <keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap function=\"control\">Ctrl</keycap><keycap>C</keycap></"
"keycombo>。如果你的物件不是路徑，那麼執行內插不會有任何作用。"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:36
msgid "Interpolation between two identical paths"
msgstr "兩個相同路徑的內插"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:37
msgid ""
"The simplest use of the Interpolate extension is to interpolate between two "
"paths that are identical. When the extension is called, the result is that "
"the space between the two paths is filled with duplicates of the original "
"paths. The number of steps defines how many of these duplicates are placed."
msgstr ""
"內插特效最簡單的用法是在兩個完全相同的路徑之間執行內插擴充功能。當呼叫這個特"
"效時，其結果便是以數個原始物件的再製物件填入這兩個路徑之間的空間。階層數會決"
"定放置多少個複製物件。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:42 tutorial-interpolate.xml:75
msgid "For example, take the following two paths:"
msgstr "例如，下面有兩個路徑："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:52
msgid ""
"Now, select the two paths, and run the Interpolate extension with the "
"settings shown in the following image."
msgstr "現在，選取這兩個路徑，然後以下面圖片裡的設定值來執行內插擴充功能。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:62
msgid ""
"As can be seen from the above result, the space between the two circle-"
"shaped paths has been filled with 6 (the number of interpolation steps) "
"other circle-shaped paths. Also note that the extension groups these shapes "
"together."
msgstr ""
"如同上圖中所見執行後的結果，兩的圓形路徑之間的地方被填入 6 個 (內插階層數) 其"
"他的圓形路徑。並注意此擴充功能會把這些形狀群組在一起。"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:69
msgid "Interpolation between two different paths"
msgstr "兩個相異路徑的內插"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:70
msgid ""
"When interpolation is done on two different paths, the program interpolates "
"the shape of the path from one into the other. The result is that you get a "
"morphing sequence between the paths, with the regularity still defined by "
"the Interpolation Steps value."
msgstr ""
"當內插用在兩個不同的路徑上時，程式所插入的路徑之形狀會從其中一個漸漸變為另一"
"個。你會獲得路徑之間連續的形狀轉變效果，形狀轉變的數量仍是由內插階層數決定。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:85
msgid ""
"Now, select the two paths, and run the Interpolate extension. The result "
"should be like this:"
msgstr "現在，選取這兩個路徑，並執行內插擴充功能。執行後的結果如下面所示："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:95
msgid ""
"As can be seen from the above result, the space between the circle-shaped "
"path and the triangle-shaped path has been filled with 6 paths that progress "
"in shape from one path to the other."
msgstr ""
"如上圖中看到的結果，圓形路徑和三角形路徑之間的地方被填入 6 個路徑，且路徑的形"
"狀從圓形漸漸轉變成三角形。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:99
msgid ""
"When using the Interpolate extension on two different paths, the "
"<emphasis>position</emphasis> of the starting node of each path is "
"important. To find the starting node of a path, select the path, then choose "
"the Node Tool so that the nodes appear and press <keycap>TAB</keycap>. The "
"first node that is selected is the starting node of that path."
msgstr ""
"當內插擴充功能用於兩個相異的路徑時，每個路徑起始節點的<emphasis>位置</"
"emphasis>就很重要。要查詢路徑的起始節點，先選取路徑，然後選擇節點工具會出現路"
"徑節點，再按 <keycap>TAB</keycap> 鍵。第一個被選取的就是路徑的起始節點。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:105
msgid ""
"See the image below, which is identical to the previous example, apart from "
"the node points being displayed. The node that is green on each path is the "
"starting node."
msgstr ""
"看下面的圖像，除了顯示節點以外其他部份和上一個範例完全相同。每個路徑上的綠色"
"節點便是起始節點。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:116
msgid ""
"The previous example (shown again below) was done with these nodes being the "
"starting node."
msgstr "上一個範例 (下圖所示) 於每個路徑上標示起始節點。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:126
msgid ""
"Now, notice the changes in the interpolation result when the triangle path "
"is mirrored so the starting node is in a different position:"
msgstr "現在，翻轉三角形路徑使得起始節點位置不同，注意內插效果產生的變化："

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:146
msgid "Interpolation Method"
msgstr "插入方式"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:147
msgid ""
"One of the parameters of the Interpolate extension is the Interpolation "
"Method. There are 2 interpolation methods implemented, and they differ in "
"the way that they calculate the curves of the new shapes. The choices are "
"either Interpolation Method 1 or 2."
msgstr ""
"插入方式是內插擴充功能的其中一個參數。有 2 種不同的插入方式，兩者的差別在於用"
"不同的方式來計算新形狀的曲線。插入方式 1 或 2 只能二選一。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:152
msgid ""
"In the examples above, we used Interpolation Method 2, and the result was:"
msgstr "在上面的範例中，我們使用插入方式 2，結果如下："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:162
msgid "Now compare this to Interpolation Method 1:"
msgstr "現在和插入方式 1 作比較："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:172
msgid ""
"The differences in how these methods calculate the numbers is beyond the "
"scope of this document, so simply try both, and use which ever one gives the "
"result closest to what you intend."
msgstr ""
"至於兩者數值計算的方式有何差別不在本文講述的範圍，所以直接試看看這兩種方式，"
"然後選用產生效果最接近你想要的那一種。"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:178
msgid "Exponent"
msgstr "指數"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:179
msgid ""
"The <firstterm>exponent parameter</firstterm> controls the spacing between "
"steps of the interpolation. An exponent of 0 makes the spacing between the "
"copies all even."
msgstr ""
"<firstterm>指數參數</firstterm>可控制插入路徑之間的間隔距離。指數為 0 會使複"
"本之間的間隔距離平均分配。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:183
msgid "Here is the result of another basic example with an exponent of 0."
msgstr "下面是另一個範例且指數為 0 的效果。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:193
msgid "The same example with an exponent of 1:"
msgstr "相同範例但指數為 1："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:203
msgid "with an exponent of 2:"
msgstr "指數為 2："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:213
msgid "and with an exponent of -1:"
msgstr "指數為 -1："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:223
msgid ""
"When dealing with exponents in the Interpolate extension, the "
"<emphasis>order</emphasis> that you select the objects is important. In the "
"examples above, the star-shaped path on the left was selected first, and the "
"hexagon-shaped path on the right was selected second."
msgstr ""
"當內插擴充功能處理指數時，選取物件的<emphasis>順序</emphasis>是很重要的。下面"
"的範例中，先選取左邊的星形路徑，再選取右邊的六邊形路徑。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:228
msgid ""
"View the result when the path on the right was selected first. The exponent "
"in this example was set to 1:"
msgstr "再看看若先選右邊路徑情形下的結果。這範例的指數設定為 1："

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:240
msgid "Duplicate Endpaths"
msgstr "再製終點路徑"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:241
msgid ""
"This parameter defines whether the group of paths that is generated by the "
"extension <emphasis>includes a copy</emphasis> of the original paths that "
"interpolate was applied on."
msgstr ""
"這個參數決定內插擴充功能生成的路徑群組是否<emphasis>包含原始路徑的複本</"
"emphasis>。"

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:247
msgid "Interpolate Style"
msgstr "插入樣式"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:248
msgid ""
"This parameter is one of the neat functions of the interpolate extension. It "
"tells the extension to attempt to change the style of the paths at each "
"step. So if the start and end paths are different colors, the paths that are "
"generated will incrementally change as well."
msgstr ""
"這個參數是內插擴充功能美妙功能的其中一項。插入樣式會使擴充功能於每階層都試著"
"改變路徑的樣式。因此如果起點路徑的顏色和終點路徑不同，生成的路徑會逐漸變成一"
"樣。"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:253
msgid ""
"Here is an example where the Interpolate Style function is used on the fill "
"of a path:"
msgstr "下面的範例中插入樣式功能被用於路徑的填色上："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:263
msgid "Interpolate Style also affects the stroke of a path:"
msgstr "插入樣式對路徑的邊框顏色也有作用："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:273
msgid ""
"Of course, the path of the start point and the end point does not have to be "
"the same either:"
msgstr "當然，起點和終點的路徑不一定要相同："

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:285
msgid "Using Interpolate to fake irregular-shaped gradients"
msgstr "用內插仿造不規則漸層"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:286
msgid ""
"At the time when gradient meshes were not implemented in Inkscape, it was "
"not possible to create a gradient other than linear (straight line) or "
"radial (round). However, it could be faked using the Interpolate extension "
"and Interpolate Style. A simple example follows — draw two lines of "
"different strokes:"
msgstr ""
"Inkscape 還無法建立線性 (直線) 或放射狀 (圓形) 以外的漸層。不過，可以用內插擴"
"充功能和插入樣式來仿造其他種類的漸層。一個簡單的例子如下 — 畫兩條不同邊框顏色"
"的線條："

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:298
msgid "And interpolate between the two lines to create your gradient:"
msgstr "然後對這兩條線段使用內插來建立漸層效果："

#. (itstool) path: sect1/title
#: tutorial-interpolate.xml:310
msgid "Conclusion"
msgstr "結論"

#. (itstool) path: sect1/para
#: tutorial-interpolate.xml:311
msgid ""
"As demonstrated above, the Inkscape Interpolate extension is a powerful "
"tool. This tutorial covers the basics of this extension, but experimentation "
"is the key to exploring interpolation further."
msgstr ""
"如同上面內容所呈現，Inkscape 的內插擴充功能是非常強大的工具。這篇教學講述了內"
"插擴充功能的基本用法，但經過實驗和練習才能完全掌握內插的特性。"

#. (itstool) path: Work/format
#: interpolate-f01.svg:49 interpolate-f02.svg:49 interpolate-f03.svg:49
#: interpolate-f04.svg:49 interpolate-f05.svg:49 interpolate-f07.svg:49
#: interpolate-f08.svg:49 interpolate-f09.svg:49 interpolate-f10.svg:49
#: interpolate-f11.svg:49 interpolate-f12.svg:49 interpolate-f13.svg:49
#: interpolate-f14.svg:49 interpolate-f15.svg:49 interpolate-f16.svg:49
#: interpolate-f17.svg:49 interpolate-f18.svg:49 interpolate-f19.svg:49
#: interpolate-f20.svg:52
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:126 interpolate-f04.svg:117 interpolate-f11.svg:117
#, no-wrap
msgid "Exponent: 0.0"
msgstr "指數: 0.0"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:130 interpolate-f04.svg:121 interpolate-f11.svg:121
#, no-wrap
msgid "Interpolation Steps: 6"
msgstr "內插階層數: 6"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:134 interpolate-f04.svg:125 interpolate-f11.svg:125
#, no-wrap
msgid "Interpolation Method: 2"
msgstr "插入方式: 2"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:138 interpolate-f04.svg:129 interpolate-f11.svg:129
#, no-wrap
msgid "Duplicate Endpaths: unchecked"
msgstr "再製終點路徑: 未勾選"

#. (itstool) path: text/tspan
#: interpolate-f02.svg:142 interpolate-f04.svg:133 interpolate-f11.svg:133
#, no-wrap
msgid "Interpolate Style: unchecked"
msgstr "插入樣式: 未勾選"

#, fuzzy
#~ msgid "ryanlerch at gmail dot com"
#~ msgstr "Ryan Lerch, ryanlerch at gmail dot com"

#~ msgid "Interpolation between two of the same path"
#~ msgstr "兩個相同路徑之間作內插"

msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Advanced\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2009-09-14 17:03+0800\n"
"Last-Translator: Liu Xiaoqin <liuxqsmile@gmail.com>\n"
"Language-Team: \n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Chinese\n"
"X-Poedit-Country: CHINA\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "柳小勤Liu Xiaoqin <liuxqsmile@gmail.com>, 2008"

#. (itstool) path: articleinfo/title
#: tutorial-advanced.xml:6
msgid "Advanced"
msgstr "高级"

#. (itstool) path: articleinfo/subtitle
#: tutorial-advanced.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-advanced.xml:11
msgid ""
"This tutorial covers copy/paste, node editing, freehand and bezier drawing, "
"path manipulation, booleans, offsets, simplification, and text tool."
msgstr ""
"本教程包括：复制/粘贴、节点编辑、手绘和Bezier曲线、路径操作、布尔操作、偏移、"
"简化、以及文本工具。"

#. (itstool) path: abstract/para
#: tutorial-advanced.xml:15
#, fuzzy
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""
"通过<keycap>Ctrl+arrows</keycap>, <keycap>滚轮</keycap>, 或者<keycap> 中键拖"
"动 </keycap>将绘图页面向下卷动。绘图对象的创建、选择、变换等基本操作，请参考"
"帮助<command>Help &gt;  教程Tutorials</command>中的基础教程。"

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:23
msgid "Pasting techniques"
msgstr "粘贴操作"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:24
#, fuzzy
msgid ""
"After you copy some object(s) by <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>C</keycap></keycombo> or cut by <keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>, the regular "
"<guimenuitem>Paste</guimenuitem> command (<keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>V</keycap></keycombo>) pastes the copied "
"object(s) right under the mouse cursor or, if the cursor is outside the "
"window, to the center of the document window. However, the object(s) in the "
"clipboard still remember the original place from which they were copied, and "
"you can paste back there by <guimenuitem>Paste In Place</guimenuitem> "
"(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap function=\"alt"
"\">Alt</keycap><keycap>V</keycap></keycombo>)."
msgstr ""
"当用<keycap>Ctrl+C</keycap>复制对象或<keycap>Ctrl+X</keycap>剪切对象后，通常"
"的<command>粘贴Paste</command>命令(<keycap>Ctrl+V</keycap>)将复制的对象粘贴到"
"鼠标光标处，如果光标在绘图窗口外，则粘贴到文档窗口的中心。实际上，剪贴板中的"
"对象仍然记着它的原始位置，你可以用<command>原位粘贴Paste in Place</command>将"
"它粘回原始位置(<keycap>Ctrl+Alt+V</keycap>)。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:34
#, fuzzy
msgid ""
"Another command, <guimenuitem>Paste Style</guimenuitem> (<keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap function=\"control\">Ctrl</"
"keycap><keycap>V</keycap></keycombo>), applies the style of the (first) "
"object on the clipboard to the current selection. The “style” thus pasted "
"includes all the fill, stroke, and font settings, but not the shape, size, "
"or parameters specific to a shape type, such as the number of tips of a star."
msgstr ""
"另一个粘贴命令，<command>粘贴样式Paste Style</command>(<keycap>Shift+Ctrl+V</"
"keycap>)，将复制对象的样式应用到所选对象。样式包括：填充、轮廓、以及字体设"
"置，但不包括形状、大小、以及与该形状相关的参数，如星形的角数等。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:41
#, fuzzy
msgid ""
"Yet another set of paste commands, <guimenuitem>Paste Size</guimenuitem>, "
"scales the selection to match the desired size attribute of the clipboard "
"object(s). There are a number of commands for pasting size and are as "
"follows: Paste Size, Paste Width, Paste Height, Paste Size Separately, Paste "
"Width Separately, and Paste Height Separately."
msgstr ""
"命令<command>粘贴大小Paste Size</command>，将复制对象的大小应用到所选对象上。"
"该命令包括：粘贴大小、宽度、高度，以及分别粘贴大小、宽度、高度。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:47
#, fuzzy
msgid ""
"<guimenuitem>Paste Size</guimenuitem> scales the whole selection to match "
"the overall size of the clipboard object(s). <guimenuitem>Paste Width</"
"guimenuitem>/<guimenuitem>Paste Height</guimenuitem> scale the whole "
"selection horizontally/vertically so that it matches the width/height of the "
"clipboard object(s). These commands honor the scale ratio lock on the "
"Selector Tool controls bar (between W and H fields), so that when that lock "
"is pressed, the other dimension of the selected object is scaled in the same "
"proportion; otherwise the other dimension is unchanged. The commands "
"containing “Separately” work similarly to the above described commands, "
"except that they scale each selected object separately to make it match the "
"size/width/height of the clipboard object(s)."
msgstr ""
"<command>粘贴大小Paste Size</command>将全部选择的总大小缩放到剪贴板中对象的总"
"大小。<command>粘贴宽度Paste Width</command>/<command>粘贴高度Paste Height</"
"command>则仅影响水平和竖直方向上的尺寸。这些命令依据复制对象的长宽比是否锁定"
"（选择工具控制栏，W和H的中间），如果复制对象的长宽比锁定，目标对象的另外一个"
"方向上的尺寸将根据该比例自动缩放；否则，另一个方向的尺寸将不改变。带有“分别"
"Separately”的相应命令也是类似的，不同之处在于将每个选择对象都分别缩放以适应复"
"制的对象。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:57
msgid ""
"Clipboard is system-wide - you can copy/paste objects between different "
"Inkscape instances as well as between Inkscape and other applications (which "
"must be able to handle SVG on the clipboard to use this)."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:63
msgid "Drawing freehand and regular paths"
msgstr "手绘和规则路径"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:64
msgid ""
"The easiest way to create an arbitrary shape is to draw it using the Pencil "
"(freehand) tool (<keycap>F6</keycap>):"
msgstr "创建任意形状的最简单的方法是使用铅笔(手绘)工具(<keycap>F6</keycap>):"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:75
#, fuzzy
msgid ""
"If you want more regular shapes, use the Pen (Bezier) tool "
"(<keycombo><keycap function=\"shift\">Shift</keycap><keycap>F6</keycap></"
"keycombo>):"
msgstr ""
"对于更规则一些的形状，可以用钢笔(Bezier)工具(<keycap>Shift+F6</keycap>):"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:86
#, fuzzy
msgid ""
"With the Pen tool, each <mousebutton role=\"click\">click</mousebutton> "
"creates a sharp node without any curve handles, so a series of clicks "
"produces a sequence of straight line segments. <mousebutton role=\"click"
"\">click</mousebutton> and <mousebutton role=\"mouse-drag\">drag</"
"mousebutton> creates a smooth Bezier node with two collinear opposite "
"handles. Press <keycap function=\"shift\">Shift</keycap> while dragging out "
"a handle to rotate only one handle and fix the other. As usual, <keycap "
"function=\"control\">Ctrl</keycap> limits the direction of either the "
"current line segment or the Bezier handles to 15 degree increments. Pressing "
"<keycap>Enter</keycap> finalizes the line, <keycap>Esc</keycap> cancels it. "
"To cancel only the last segment of an unfinished line, press "
"<keycap>Backspace</keycap>."
msgstr ""
"在钢笔工具中，每次<keycap>单击</keycap>创建一个没有曲线控制柄的尖锐点，所以，"
"一系列的单击产生一串线段，<keycap>点击然后拖动</keycap>产生一个光滑的Bezier节"
"点，两边各有一个共线的控制柄。拖动一个控制柄时，按住<keycap>Shift</keycap>可"
"以保持另一个不动。同样，<keycap>Ctrl</keycap>限制当前线段或Bezier控制柄的方向"
"为15度的整数倍。<keycap>Enter</keycap>结束绘制，<keycap>Esc</keycap>取消。如"
"果只取消上一段，使用<keycap>Backspace</keycap>。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:96
msgid ""
"In both freehand and bezier tools, the currently selected path displays "
"small square <firstterm>anchors</firstterm> at both ends. These anchors "
"allow you to <emphasis>continue</emphasis> this path (by drawing from one of "
"the anchors) or <emphasis>close</emphasis> it (by drawing from one anchor to "
"the other) instead of creating a new one."
msgstr ""
"在手绘和bezier工具模式下，选中路径的两端都会显示一个方形的<firstterm>锚点"
"anchors</firstterm>，在这些锚点上可以继续绘图，从而延长路径，或使其<emphasis>"
"封闭</emphasis>（从一个锚点画到另一个锚点），而不产生新的路径。"

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:104
msgid "Editing paths"
msgstr "编辑路径"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:105
msgid ""
"Unlike shapes created by shape tools, the Pen and Pencil tools create what "
"is called <firstterm>paths</firstterm>. A path is a sequence of straight "
"line segments and/or Bezier curves which, as any other Inkscape object, may "
"have arbitrary fill and stroke properties. But unlike a shape, a path can be "
"edited by freely dragging any of its nodes (not just predefined handles) or "
"by directly dragging a segment of the path. Select this path and switch to "
"the Node tool (<keycap>F2</keycap>):"
msgstr ""
"形状工具创建的是形状，而钢笔和铅笔工具创建的是<firstterm>路径</firstterm>。路"
"径由直线和Bezier曲线构成，像其他对象一样，路径也可以设置任意类型的填充和轮廓"
"属性。但与形状不同的是，修改路径时可以随意调整节点和(直线或曲线)段，而不是预"
"先设置好的控制柄。切换到节点工具(<keycap>F2</keycap>)，然后选择下面的路径："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:119
#, fuzzy
msgid ""
"You will see a number of gray square <firstterm>nodes</firstterm> on the "
"path. These nodes can be <firstterm>selected</firstterm> by <mousebutton "
"role=\"click\">click</mousebutton>, <keycombo><keycap function=\"shift"
"\">Shift</keycap><mousebutton role=\"click\">click</mousebutton></keycombo>, "
"or by <mousebutton role=\"mouse-drag\">drag</mousebutton>ging a rubberband - "
"exactly like objects are selected by the Selector tool. You can also click a "
"path segment to automatically select the adjacent nodes. Selected nodes "
"become highlighted and show their <firstterm>node handles</firstterm> - one "
"or two small circles connected to each selected node by straight lines. The "
"<keycap>!</keycap> key inverts node selection in the current subpath(s) (i."
"e. subpaths with at least one selected node); <keycombo><keycap function="
"\"alt\">Alt</keycap><keycap>!</keycap></keycombo> inverts in the entire path."
msgstr ""
"你会看到路径上有一些灰色的方形<firstterm>节点</firstterm>。通过<keycap>点击</"
"keycap>、<keycap>Shift+点击</keycap>、或<keycap>拖出</keycap>弹性选框，来选择"
"这些节点，与选择器工具拾取对象完全相同。也可以单击路径中的一段来选择相邻的节"
"点。选中的节点将高亮显示，并出现<firstterm>节点控制柄</firstterm>：一个或两个"
"与该节点相连的小圆圈。<keycap>!</keycap>键在当前子路径范围内反选节点(子路径上"
"至少选中一个节点)；<keycap>Alt+!</keycap>在整个路径范围内反选节点。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:130
#, fuzzy
msgid ""
"Paths are edited by <mousebutton role=\"mouse-drag\">drag</mousebutton>ging "
"their nodes, node handles, or directly dragging a path segment. (Try to drag "
"some nodes, handles, and path segments of the above path.) <keycap function="
"\"control\">Ctrl</keycap> works as usual to restrict movement and rotation. "
"The <keycap>arrow</keycap> keys, <keycap>Tab</keycap>, <keycap>[</keycap>, "
"<keycap>]</keycap>, <keycap>&lt;</keycap>, <keycap>&gt;</keycap> keys with "
"their modifiers all work just as they do in selector, but apply to nodes "
"instead of objects. You can add nodes anywhere on a path by either double "
"clicking or by <keycombo><keycap function=\"control\">Ctrl</keycap><keycap "
"function=\"alt\">Alt</keycap><mousebutton role=\"click\">click</"
"mousebutton></keycombo> at the desired location."
msgstr ""
"路径的编辑通过拖动节点、节点控制柄、或路径段来进行。请在上面的路径上练习一"
"下。<keycap>Ctrl</keycap>仍然有限制移动和旋转的作用。<keycap>光标arrow</"
"keycap> 键, <keycap>Tab</keycap>, <keycap>[</keycap>, <keycap>]</keycap>, "
"<keycap>&lt;</keycap>, <keycap>&gt;</keycap>的作用与选择工具中一样，但作用于"
"节点，而不是整个绘图对象。在路径上的任意位置双击或<keycap>Ctrl+Alt+Click</"
"keycap>可以添加节点。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:140
#, fuzzy
msgid ""
"You can delete nodes with <keycap>Del</keycap> or <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap function=\"alt\">Alt</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo>. When deleting nodes it will "
"try to retain the shape of the path, if you desire for the handles of the "
"adjacent nodes to be retracted (not retaining the shape) you can delete with "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>Del</keycap></"
"keycombo>. Additionally, you can duplicate (<keycombo><keycap function="
"\"shift\">Shift</keycap><keycap>D</keycap></keycombo>) selected nodes. The "
"path can be broken (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap>B</keycap></keycombo>) at the selected nodes, or if you "
"select two endnodes on one path, you can join them (<keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>J</keycap></keycombo>)."
msgstr ""
"选中节点后按<keycap>Del</keycap> ，或者<keycap>Ctrl+Alt+单击</keycap>一个节"
"点，可以将其删去。删除节点时，将尽可能保持路径的形状，如果要保持相邻的节点不"
"变（形状将发生变化），用<keycap>Ctrl+Del</keycap>删除。另外，<keycap>Shift"
"+D</keycap>可以再制选中的节点，<keycap>Shift+B</keycap>可以将路径在选中的节点"
"处打开，如果选中的是路径的两个终点，则可以将其结合在一起。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:151
#, fuzzy
msgid ""
"A node can be made <firstterm>cusp</firstterm> (<keycombo><keycap function="
"\"shift\">Shift</keycap><keycap>C</keycap></keycombo>), which means its two "
"handles can move independently at any angle to each other; "
"<firstterm>smooth</firstterm> (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap>S</keycap></keycombo>), which means its handles are always on "
"the same straight line (collinear); <firstterm>symmetric</firstterm> "
"(<keycombo><keycap function=\"shift\">Shift</keycap><keycap>Y</keycap></"
"keycombo>), which is the same as smooth, but the handles also have the same "
"length; and <firstterm>auto-smooth</firstterm> (<keycombo><keycap function="
"\"shift\">Shift</keycap><keycap>A</keycap></keycombo>), a special node that "
"automatically adjusts the handles of the node and surrounding auto-smooth "
"nodes to maintain a smooth curve. When you switch the type of node, you can "
"preserve the position of one of the two handles by hovering your "
"<mousebutton>mouse</mousebutton> over it, so that only the other handle is "
"rotated/scaled to match."
msgstr ""
"<keycap>Shift+C</keycap>可以使节点<firstterm>尖锐Cusp</firstterm> ，它的两个"
"控制柄独立，可以各自调整角度；<keycap>Shift+S</keycap>可以使节点<firstterm>平"
"滑smooth</firstterm> ，两个控制柄共线；<keycap>Shift+Y</keycap>可以让节点"
"<firstterm>对称symmetric</firstterm> ，两个控制柄共线并且等长。改变节点的类型"
"时，将鼠标悬停在一个控制柄上，可以保持该控制柄不变，仅另一个控制柄相应地改"
"变。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:164
#, fuzzy
msgid ""
"Also, you can <firstterm>retract</firstterm> a node's handle altogether by "
"<keycombo><keycap function=\"control\">Ctrl</keycap><mousebutton role=\"click"
"\">click</mousebutton></keycombo>ing on it. If two adjacent nodes have their "
"handles retracted, the path segment between them is a straight line. To pull "
"out the retracted node, <keycombo><keycap function=\"shift\">Shift</"
"keycap><mousebutton role=\"mouse-drag\">drag</mousebutton></keycombo> away "
"from the node."
msgstr ""
"通过<keycap>Ctrl+click</keycap>控制柄，可以将节点的控制柄收回（到节点上），如"
"果相邻两个节点的控制柄都被收回，它们中间将变为直线。在节点上<keycap>Shift"
"+drag</keycap>可以将控制柄重新拉出。"

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:174
msgid "Subpaths and combining"
msgstr "子路径和结合"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:175
msgid ""
"A path object may contain more than one <firstterm>subpath</firstterm>. A "
"subpath is a sequence of nodes connected to each other. (Therefore, if a "
"path has more than one subpath, not all of its nodes are connected.) Below "
"left, three subpaths belong to a single compound path; the same three "
"subpaths on the right are independent path objects:"
msgstr ""
"一个路径可以包含数个<firstterm>子路径subpath</firstterm>。每个子路径中的节点"
"互相连接，子路径与子路径之间则是断开的。左下图，三个子路径组合为一个路径，右"
"下图中则互相独立，各自为一个路径："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:188
msgid ""
"Note that a compound path is not the same as a group. It's a single object "
"which is only selectable as a whole. If you select the left object above and "
"switch to node tool, you will see nodes displayed on all three subpaths. On "
"the right, you can only node-edit one path at a time."
msgstr ""
"要注意的是，复合路径并不等同于群组，它是一个单独的对象。如果你选中左上的对"
"象，然后切换到节点工具，将会看到，三个子路径上的节点都显现出来，而在右侧，每"
"次只能选中一个路径进行节点编辑。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:193
#, fuzzy
msgid ""
"Inkscape can <guimenuitem>Combine</guimenuitem> paths into a compound path "
"(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>K</keycap></"
"keycombo>) and <guimenuitem>Break Apart</guimenuitem> a compound path into "
"separate paths (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>K</keycap></keycombo>). Try these "
"commands on the above examples. Since an object can only have one fill and "
"stroke, a new compound path gets the style of the first (lowest in z-order) "
"object being combined."
msgstr ""
"通过对几个路径进行<command>结合Combine</command>可以形成一个复合路径"
"(<keycap>Ctrl+K</keycap>)，也可以将一个复合路径分解为几个独立的路径 "
"(<keycap>Shift+Ctrl+K</keycap>)。在上图中练习一下。由于一个对象只能有一种填充"
"和轮廓样式，结合后的复合路径将继承第一个对象（处于叠放次序的底层）的属性。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:201
msgid ""
"When you combine overlapping paths with fill, usually the fill will "
"disappear in the areas where the paths overlap:"
msgstr ""
"在合并有填充的路径时，如果路径之间有重叠区域，合并后，重叠部分的填充将消失："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:212
msgid ""
"This is the easiest way to create objects with holes in them. For more "
"powerful path commands, see “Boolean operations” below."
msgstr ""
"这是创建内部有孔的形状的最简单的方法。路径工具的高级操作请参考下面的“布尔操"
"作”。"

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:218
msgid "Converting to path"
msgstr "转换为路径"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:219
#, fuzzy
msgid ""
"Any shape or text object can be <firstterm>converted to path</firstterm> "
"(<keycombo><keycap function=\"shift\">Shift</keycap><keycap function="
"\"control\">Ctrl</keycap><keycap>C</keycap></keycombo>). This operation does "
"not change the appearance of the object but removes all capabilities "
"specific to its type (e.g. you can't round the corners of a rectangle or "
"edit the text anymore); instead, you can now edit its nodes. Here are two "
"stars - the left one is kept a shape and the right one is converted to path. "
"Switch to node tool and compare their editability when selected:"
msgstr ""
"任何的形状和文本都可以<firstterm>转为路径</firstterm> (<keycap>Shift+Ctrl+C</"
"keycap>)。转换不改变对象的外观，但对象原本所具有的特殊编辑方式（例如矩形倒"
"圆，改变文本内容等）都将不复存在，而变为用节点工具进行编辑。这里有两个星形，"
"左边的一个是形状，右边的已经转为路径，切换到节点工具模式，选择这两个对象，看"
"看他们的区别："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:234
#, fuzzy
msgid ""
"Moreover, you can convert to a path (“outline”) the <firstterm>stroke</"
"firstterm> of any object. Below, the first object is the original path (no "
"fill, black stroke), while the second one is the result of the "
"<guimenuitem>Stroke to Path</guimenuitem> command (black fill, no stroke):"
msgstr ""
"而且，任何对象的<firstterm>轮廓stroke</firstterm>都可以转换为路径"
"(“outline”)。下图中第一个是原始路径（无填充，黑色轮廓），第二个是执行"
"<commands>轮廓转为路径Stroke to Path</commands>后（黑色填充，无轮廓）："

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:248
msgid "Boolean operations"
msgstr "布尔操作"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:249
#, fuzzy
msgid ""
"The commands in the <guimenu>Path</guimenu> menu let you combine two or more "
"objects using <firstterm>boolean operations</firstterm>:"
msgstr ""
"路径Path菜单中命令可以将多个路径以<firstterm>布尔操作boolean operations</"
"firstterm>的方式结合到一起："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:260
#, fuzzy
msgid ""
"The keyboard shortcuts for these commands allude to the arithmetic analogs "
"of the boolean operations (union is addition, difference is subtraction, "
"etc.). The <guimenuitem>Difference</guimenuitem> and <guimenuitem>Exclusion</"
"guimenuitem> commands can only apply to two selected objects; others may "
"process any number of objects at once. The result always receives the style "
"of the bottom object."
msgstr ""
"布尔操作对应的快捷键也与相应的运算相适应(合并union对应加号，相减difference对"
"应减号，等)。命令<command>相减Difference</command>和 <command>排除Exclusion</"
"command> 只针对两个路径，其它操作可以应用于任意数量的对象。操作后的对象总是保"
"留参与操作的底层对象的样式。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:266
#, fuzzy
msgid ""
"The result of the <guimenuitem>Exclusion</guimenuitem> command looks similar "
"to <guimenuitem>Combine</guimenuitem> (see above), but it is different in "
"that <guimenuitem>Exclusion</guimenuitem> adds extra nodes where the "
"original paths intersect. The difference between <guimenuitem>Division</"
"guimenuitem> and <guimenuitem>Cut Path</guimenuitem> is that the former cuts "
"the entire bottom object by the path of the top object, while the latter "
"only cuts the bottom object's stroke and removes any fill (this is "
"convenient for cutting fill-less strokes into pieces)."
msgstr ""
"<command>排除Exclusion</command> 与<command>结合Combine</command> 操作有些类"
"似，只不过，<command>排除Exclusion</command> 在原始对象相交的地方添加节点。"
"<command>分割Division</command> 和 <command>剪切路径Cut Path</command>命令的"
"区别在于前者用顶层路径将底层路径完全剪切，而后者只剪切轮廓，填充则完全完全删"
"除(适用于将不用填充的轮廓分为数段)。"

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:276
msgid "Inset and outset"
msgstr "嵌入与扩展"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:277
#, fuzzy
msgid ""
"Inkscape can expand and contract shapes not only by scaling, but also by "
"<firstterm>offsetting</firstterm> an object's path, i.e. by displacing it "
"perpendicular to the path in each point. The corresponding commands are "
"called <guimenuitem>Inset</guimenuitem> (<keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>(</keycap></keycombo>) and <guimenuitem>Outset</"
"guimenuitem> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>)</"
"keycap></keycombo>). Shown below is the original path (red) and a number of "
"paths inset or outset from that original:"
msgstr ""
"Inscape不仅可以通过缩放，也可以通过<firstterm>偏移offsetting</firstterm>来扩"
"展和收缩形状，即将路径上的点沿法线方向移动。相应的命令为：<command>嵌入"
"Inset</command> (<keycap>Ctrl+(</keycap>) 和 <command>扩展Outset</command> "
"(<keycap>Ctrl+)</keycap>)。下图中给出了原始路径(红色)以及通过嵌入和扩展产生的"
"新路径："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:293
#, fuzzy
msgid ""
"The plain <guimenuitem>Inset</guimenuitem> and <guimenuitem>Outset</"
"guimenuitem> commands produce paths (converting the original object to path "
"if it's not a path yet). Often, more convenient is the <guimenuitem>Dynamic "
"Offset</guimenuitem> (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>J</keycap></keycombo>) which creates an object with a "
"draggable handle (similar to a shape's handle) controlling the offset "
"distance. Select the object below, switch to the node tool, and drag its "
"handle to get an idea:"
msgstr ""
"正常情况下，<command>嵌入Inset</command> 和<command>扩展Outset</command>命令"
"生成的对象是路径（如果原始对象不是路径，将先转为路径）。通常，更方便的命令是"
"<command>动态偏移Dynamic Offset</command> (<keycap>Ctrl+J</keycap>)，通过一个"
"拖动控制柄（同形状的控制柄类似）来控制偏移量。选中下面的对象，切换到节点工"
"具，拖动控制柄到一个合适的位置："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:308
msgid ""
"Such a <firstterm>dynamic offset object</firstterm> remembers the original "
"path, so it does not “degrade” when you change the offset distance again and "
"again. When you don't need it to be adjustable anymore, you can always "
"convert an offset object back to path."
msgstr ""
"这种<firstterm>动态偏移对象dynamic offset object</firstterm>会记录原始位置，"
"多次调整偏移时不会产生退化(degrade)。如果不需要再调整，可以将偏移对象转为路"
"径。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:313
msgid ""
"Still more convenient is a <firstterm>linked offset</firstterm>, which is "
"similar to the dynamic variety but is connected to another path which "
"remains editable. You can have any number of linked offsets for one source "
"path. Below, the source path is red, one offset linked to it has black "
"stroke and no fill, the other has black fill and no stroke."
msgstr ""
"也许，更有效的是<firstterm>关联偏移linked offset</firstterm>，与动态偏移类"
"似，但原始对象仍然保留，并且可以编辑。一个原始对象可以有多个关联偏移。下图"
"中，原始对象是红色的，其中一个关联偏移轮廓是黑色的，没有填充，另一个有黑色填"
"充，但没有轮廓。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:319
#, fuzzy
msgid ""
"Select the red object and node-edit it; watch how both linked offsets "
"respond. Now select any of the offsets and drag its handle to adjust the "
"offset radius. Finally, notehow you can move or transform the offset objects "
"independently without losing their connection with the source."
msgstr ""
"选择红色的对象，编辑其节点，观察关联偏移对象的变化。选择关联对象，拖动控制"
"柄，调节偏移量。你会注意到，移动和改变原始对象影响到关联偏移对象，而偏移对象"
"的移动和变换是独立的，同时保持和源对象的链接关系。"

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:332
msgid "Simplification"
msgstr "简化"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:333
#, fuzzy
msgid ""
"The main use of the <guimenuitem>Simplify</guimenuitem> command "
"(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>) is reducing the number of nodes on a path while <emphasis>almost</"
"emphasis> preserving its shape. This may be useful for paths created by the "
"Pencil tool, since that tool sometimes creates more nodes than necessary. "
"Below, the left shape is as created by the freehand tool, and the right one "
"is a copy that was simplified. The original path has 28 nodes, while the "
"simplified one has 17 (which means it is much easier to work with in node "
"tool) and is smoother."
msgstr ""
"<command>简化Simplify</command> (<keycap>Ctrl+L</keycap>)命令在<emphasis>尽量"
"保持形状</emphasis>的情况下减少路径上的节点。铅笔工具创建的对象，节点数目往往"
"过多，需要这个工具来简化。下图中，左侧的形状是通过手绘工具创建的，右侧是简化"
"后的。原始对象有28个节点，简化后只有17个(节点工具编辑时更容易一些)，而且更平"
"滑。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:348
#, fuzzy
msgid ""
"The amount of simplification (called the <firstterm>threshold</firstterm>) "
"depends on the size of the selection. Therefore, if you select a path along "
"with some larger object, it will be simplified more aggressively than if you "
"select that path alone. Moreover, the <guimenuitem>Simplify</guimenuitem> "
"command is <firstterm>accelerated</firstterm>. This means that if you press "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo> several times in quick succession (so that the calls are within "
"0.5 sec from each other), the threshold is increased on each call. (If you "
"do another Simplify after a pause, the threshold is back to its default "
"value.) By making use of the acceleration, it is easy to apply the exact "
"amount of simplification you need for each case."
msgstr ""
"简化的程度（称为<firstterm>阈值threshold</firstterm>）取决于选区的大小。所"
"以，如果选择路径的同时也选择了较大对象，简化的程度将更大。并且，<command>简化"
"</command>的速度将<firstterm>加快</firstterm>。也就是所，如果连着按几次"
"<keycap>Ctrl+L</keycap>（间隔不超过0.5秒），每次简化的阈值将递增。（如果等一"
"会再执行，阈值又会还原原始大小。）通过这种方法可以比较精确地控制简化的程度。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:358
#, fuzzy
msgid ""
"Besides smoothing freehand strokes, <guimenuitem>Simplify</guimenuitem> can "
"be used for various creative effects. Often, a shape which is rigid and "
"geometric benefits from some amount of simplification that creates cool life-"
"like generalizations of the original form - melting sharp corners and "
"introducing very natural distortions, sometimes stylish and sometimes plain "
"funny. Here's an example of a clipart shape that looks much nicer after "
"<guimenuitem>Simplify</guimenuitem>:"
msgstr ""
"除了平滑手绘对象，<command>简化</command>命令还可以产生许多创造性的效果。显得"
"尖锐和呆板的对象经过简化经常后产生更柔和的效果：锐角变得平滑，引入更自然的变"
"形效果，显得更生动，更有风格。下面是一个剪贴画对象经过<command>简化</command>"
"后的效果："

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:374
msgid "Creating text"
msgstr "创建文本"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:375
msgid ""
"Inkscape is capable of creating long and complex texts. However, it's also "
"pretty convenient for creating small text objects such as heading, banners, "
"logos, diagram labels and captions, etc. This section is a very basic "
"introduction into Inkscape's text capabilities."
msgstr ""
"Inkscape可以创建复杂的文本。也可以很方便地绘制简短的文字对象，例如标题，标"
"识，标语，流程图等中的文字。本节介绍Inkscape中文本工具的基本功能。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:380
#, fuzzy
msgid ""
"Creating a text object is as simple as switching to the Text tool "
"(<keycap>F8</keycap>), clicking somewhere in the document, and typing your "
"text. To change font family, style, size, and alignment, open the Text and "
"Font dialog (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>T</keycap></keycombo>). That "
"dialog also has a text entry tab where you can edit the selected text object "
"- in some situations, it may be more convenient than editing it right on the "
"canvas (in particular, that tab supports as-you-type spell checking)."
msgstr ""
"切换到文本工具(<keycap>F8</keycap>)，在页面上的任意位置点击，然后输入文字。打"
"开文本和字体对话框Text and Font dialog(<keycap>Shift+Ctrl+T</keycap>)，可以修"
"改文字的字体，样式，大小和对齐方式。这个对话框里也有一个文字输入框，可以修改"
"选中的文本的内容。在这个对话框里输入文本可能比在画布上更方便（而且支持拼写检"
"查）。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:389
#, fuzzy
msgid ""
"Like other tools, Text tool can select objects of its own type - text "
"objects -so you can click to select and position the cursor in any existing "
"text object (such as this paragraph)."
msgstr ""
"像其它工具一样，文本工具模式下可以选择其自身类型的对象——文本对象——点击选择，"
"将输入光标放到文本中的任意位置（比如这个段落）。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:393
#, fuzzy
msgid ""
"One of the most common operations in text design is adjusting spacing "
"between letters and lines. As always, Inkscape provides keyboard shortcuts "
"for this. When you are editing text, the <keycombo><keycap function=\"alt"
"\">Alt</keycap><keycap>&lt;</keycap></keycombo> and <keycombo><keycap "
"function=\"alt\">Alt</keycap><keycap>&gt;</keycap></keycombo> keys change "
"the <firstterm>letter spacing</firstterm> in the current line of a text "
"object, so that the total length of the line changes by 1 pixel at the "
"current zoom (compare to Selector tool where the same keys do pixel-sized "
"object scaling). As a rule, if the font size in a text object is larger than "
"the default, it will likely benefit from squeezing letters a bit tighter "
"than the default. Here's an example:"
msgstr ""
"文本编辑中常用的一个操作是调整文字间距和行间距，Inkscape中同样有对应的键盘操"
"作方式。当编辑文本时，<keycap>Alt+&lt;</keycap> 和<keycap>Alt+&gt;</keycap>改"
"变当前行的<firstterm>字间距letter spacing</firstterm>，该行的长度在当前缩放级"
"别上每次改变一个像素（选择工具中，同样用这些键实现像素级别的缩放）。通常，如"
"果字体比默认的大，字间距紧凑一些看起来更协调。例如："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:410
msgid ""
"The tightened variant looks a bit better as a heading, but it's still not "
"perfect: the distances between letters are not uniform, for example the “a” "
"and “t” are too far apart while “t” and “i” are too close. The amount of "
"such bad kerns (especially visible in large font sizes) is greater in low "
"quality fonts than in high quality ones; however, in any text string and in "
"any font you will probably find pairs of letters that will benefit from "
"kerning adjustments."
msgstr ""
"紧凑一些的作为标题看起来更好一些，但仍然不是很完美：字间距并不一致，例如，“a"
"\"和\"t\"的间隔比\"t\"和\"i\"的间距大。在一些质量比较差的字体中，（尤其是字体"
"比较大的情况下）这种不均衡的紧排更明显；但是，不管任何字体，总会存在这种文本"
"组合，需要手工调整松紧。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:417
#, fuzzy
msgid ""
"Inkscape makes these adjustments really easy. Just move your text editing "
"cursor between the offending characters and use <keycombo><keycap function="
"\"alt\">Alt</keycap><keycap>arrows</keycap></keycombo> to move the letters "
"right of the cursor. Here is the same heading again, this time with manual "
"adjustments for visually uniform letter positioning:"
msgstr ""
"在Inkscape中调整起来是很方便的，将光标放到需要调整的两个字符的中间，"
"<keycap>Alt+arrows</keycap>键移动光标右侧的文字。与上面相同的文字，手动调整字"
"符间距后："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:430
#, fuzzy
msgid ""
"In addition to shifting letters horizontally by <keycombo><keycap function="
"\"alt\">Alt</keycap><keycap>Left</keycap></keycombo> or <keycombo><keycap "
"function=\"alt\">Alt</keycap><keycap>Right</keycap></keycombo>, you can also "
"move them vertically by using <keycombo><keycap function=\"alt\">Alt</"
"keycap><keycap>Up</keycap></keycombo> or <keycombo><keycap function=\"alt"
"\">Alt</keycap><keycap>Down</keycap></keycombo>:"
msgstr ""
"除了<keycap>Alt+Left</keycap> 和<keycap>Alt+Right</keycap>将文字左右移动，"
"<keycap>Alt+Up</keycap> 和 <keycap>Alt+Down</keycap>也可以将文字上下移动："

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:444
#, fuzzy
msgid ""
"Of course you could just convert your text to path (<keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap function=\"control\">Ctrl</"
"keycap><keycap>C</keycap></keycombo>) and move the letters as regular path "
"objects. However, it is much more convenient to keep text as text - it "
"remains editable, you can try different fonts without removing the kerns and "
"spacing, and it takes much less space in the saved file. The only "
"disadvantage to the “text as text” approach is that you need to have the "
"original font installed on any system where you want to open that SVG "
"document."
msgstr ""
"当然也可以将文字转为路径(<keycap>Shift+Ctrl+C</keycap>)，并将字符像路径一样移"
"动。但是，让其保持为文字无疑是更好的选择，不仅可以编辑，改变字体时也不会丢失"
"间距，文件的体积也更小。保留为文本的唯一缺点是，当你将该SVG文件拿到别的计算机"
"上打开时，这个机子上必须安装有相应的字体。"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:452
#, fuzzy
msgid ""
"Similar to letter spacing, you can also adjust <firstterm>line spacing</"
"firstterm> in multi-line text objects. Try the <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap function=\"alt\">Alt</keycap><keycap>&lt;</"
"keycap></keycombo> and <keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>&gt;</keycap></keycombo> "
"keys on any paragraph in this tutorial to space it in or out so that the "
"overall height of the text object changes by 1 pixel at the current zoom. As "
"in Selector, pressing <keycap function=\"shift\">Shift</keycap> with any "
"spacing or kerning shortcut produces 10 times greater effect than without "
"Shift."
msgstr ""
"与字间距类似，在多行文本中也可以调整<firstterm>行间距line spacing</"
"firstterm>。在本教程的任意段落中，<keycap>Ctrl+Alt+&lt;</keycap> 和"
"<keycap>Ctrl+Alt+&gt;</keycap>来增大和缩小行间距，每次调整，整个文本的高度在"
"当前缩放级别上改变一个像素。与选择工具类似，配合<keycap>Shift</keycap>键，行"
"间距和字间距的调整量扩大十倍。"

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:464
msgid "XML editor"
msgstr "XML编辑器"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:465
#, fuzzy
msgid ""
"The ultimate power tool of Inkscape is the XML editor (<keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap function=\"control\">Ctrl</"
"keycap><keycap>X</keycap></keycombo>). It displays the entire XML tree of "
"the document, always reflecting its current state. You can edit your drawing "
"and watch the corresponding changes in the XML tree. Moreover, you can edit "
"any text, element, or attribute nodes in the XML editor and see the result "
"on your canvas. This is the best tool imaginable for learning SVG "
"interactively, and it allows you to do tricks that would be impossible with "
"regular editing tools."
msgstr ""
"Inkscape中的终极工具是XML编辑器(<keycap>Shift+Ctrl+X</keycap>)，可以实时显示"
"整个文档的XML树形图。修改绘图时，你可以注意一下XML树形图中的变化。也可以在XML"
"编辑器中修改文本、元素或者节点属性，然后在画图上查看效果。这是一个非常形象化"
"的学习SVG格式的交互式工具。并且可以实现一些通常的编辑工具无法完成的功能。"

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:475
msgid "Conclusion"
msgstr "小结"

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:476
msgid ""
"This tutorial shows only a small part of all capabilities of Inkscape. We "
"hope you enjoyed it. Don't be afraid to experiment and share what you "
"create. Please visit <ulink url=\"http://www.inkscape.org\">www.inkscape."
"org</ulink> for more information, latest versions, and help from user and "
"developer communities."
msgstr ""
"这个教程只展示了Inkscape功能的一小部分，我们希望你能喜欢。欢迎探索它的功能，"
"展示你的灵感。更多信息，最新版本，以及寻求用户社区的帮助，请登录<ulink url="
"\"http://www.inkscape.org\">www.inkscape.org</ulink>。"

#. (itstool) path: Work/format
#: advanced-f01.svg:49 advanced-f02.svg:48 advanced-f03.svg:48
#: advanced-f04.svg:48 advanced-f05.svg:48 advanced-f06.svg:48
#: advanced-f07.svg:48 advanced-f08.svg:49 advanced-f09.svg:48
#: advanced-f10.svg:48 advanced-f11.svg:49 advanced-f12.svg:48
#: advanced-f13.svg:48 advanced-f14.svg:48 advanced-f15.svg:48
#: advanced-f16.svg:48
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: advanced-f08.svg:70
#, no-wrap
msgid "Original shapes"
msgstr "原始形状"

#. (itstool) path: text/tspan
#: advanced-f08.svg:81
#, no-wrap
msgid "Union"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:86
#, fuzzy, no-wrap
msgid "(Ctrl++)"
msgstr "(Ctrl+*)"

#. (itstool) path: text/tspan
#: advanced-f08.svg:97
#, no-wrap
msgid "Difference (Ctrl+-)"
msgstr "相减Difference (Ctrl+-)"

#. (itstool) path: text/tspan
#: advanced-f08.svg:102
#, no-wrap
msgid "(bottom minus top)"
msgstr "(底部减去顶部)"

#. (itstool) path: text/tspan
#: advanced-f08.svg:113
#, no-wrap
msgid "Intersection"
msgstr "交集Intersection"

#. (itstool) path: text/tspan
#: advanced-f08.svg:118
#, no-wrap
msgid "(Ctrl+*)"
msgstr "(Ctrl+*)"

#. (itstool) path: text/tspan
#: advanced-f08.svg:129
#, no-wrap
msgid "Exclusion"
msgstr "排除Exclusion"

#. (itstool) path: text/tspan
#: advanced-f08.svg:134
#, no-wrap
msgid "(Ctrl+^)"
msgstr "(Ctrl+^)"

#. (itstool) path: text/tspan
#: advanced-f08.svg:145
#, no-wrap
msgid "Division"
msgstr "分割Division"

#. (itstool) path: text/tspan
#: advanced-f08.svg:150
#, no-wrap
msgid "(Ctrl+/)"
msgstr "(Ctrl+/)"

#. (itstool) path: text/tspan
#: advanced-f08.svg:161
#, no-wrap
msgid "Cut Path"
msgstr "剪切路径Cut Path"

#. (itstool) path: text/tspan
#: advanced-f08.svg:166
#, no-wrap
msgid "(Ctrl+Alt+/)"
msgstr "(Ctrl+Alt+/)"

#. (itstool) path: text/tspan
#: advanced-f13.svg:69 advanced-f14.svg:69
#, no-wrap
msgid "Original"
msgstr "原始的"

#. (itstool) path: text/tspan
#: advanced-f13.svg:80
#, no-wrap
msgid "Slight simplification"
msgstr "轻微简化"

#. (itstool) path: text/tspan
#: advanced-f13.svg:91
#, no-wrap
msgid "Aggressive simplification"
msgstr "严重简化"

#. (itstool) path: text/tspan
#: advanced-f14.svg:80
#, no-wrap
msgid "Letter spacing decreased"
msgstr "字间距减小，"

#. (itstool) path: text/tspan
#: advanced-f14.svg:87 advanced-f14.svg:95 advanced-f15.svg:78
#: advanced-f16.svg:68
#, no-wrap
msgid "Inspiration"
msgstr "Inspiration"

#. (itstool) path: text/tspan
#: advanced-f15.svg:69
#, no-wrap
msgid "Letter spacing decreased, some letter pairs manually kerned"
msgstr "字间距减小，某些字之间手工调节"

#~ msgid "Union (Ctrl++)"
#~ msgstr "合并Union (Ctrl++)"

#~ msgid ""
#~ "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
#~ msgstr ""
#~ "bulia byak, buliabyak@users.sf.net ； josh andler, scislac@users.sf.net"

#~ msgid ""
#~ "Note that Inkscape has its own internal clipboard; it does not use the "
#~ "system clipboard except for copying/pasting text by the Text tool."
#~ msgstr ""
#~ "注意，Inkscape使用自己的内部剪贴板，除了在文本工具中复制/粘贴文本外，不使"
#~ "用操作系统的剪贴板。"
